﻿var i = 0;
var aantalgoed = 0;
var goedantwoord;
var trues = false;
var first = 0;
var Testje;
var Testquiz;
var Answerdata;
var a = 0;
var totalevraag;
var quistionid;
var answerid;
var antwoorden;
var aantalantwoorden;
var q;
var cources;
var idofquiz;

//Laad quiz eerste keer in
function OnLoad() {
	authenticate_User_andGet_Token();
}

//Bewaar id van geselecteerde quiz
function Clickedlink(id) {
	localStorage.Id = id;
}

//Begin met quiz nog een keer quiz inladen en token krijgen
function Click(clicked_id) {

	authenticate_User_andGet_Token();
	AddQuistion_and_Check_Answers(clicked_id);
}

//check of het gegeven antwoord goed is
function Answer(a, data) {

	antwoorden = data["question"]["answers"];
	//voor elke gebruiker
	aantalantwoorden = objectLength(antwoorden);
	for (var i = 0; i < aantalantwoorden; i++) {
		var correct = antwoorden[i]["correct"];
		if (correct == true) {
			var correctanswer = antwoorden[i]["id"];
			var givenanswer = a;
			if (correctanswer == givenanswer) {
				aantalgoed++;
				question = true;
			}
		}
	}
}

//voeg vragen aan html pagina toe en selecteer gegven antwoord
function AddQuistion_and_Check_Answers(clicked_id) {
	//get token
	authenticate_User_andGet_Token();
	var button = parseInt(clicked_id);

	var aantalvragen = objectLength(Testquiz);

	//Bij elke als je nog niet bij laatste vraag bent doe je dit
	if (i < aantalvragen + 1) {
		totalevraag = Testquiz[i];

		//probeer antwoord te checken
		try {
			quistionid = Testquiz[i - 1]["id"];
			var allanswers;
			var aantalanswers;
			try {
				allanswers = Testquiz[i - 1]["answers"];
				aantalanswers = allanswers["length"];
			}
			catch{
				allanswers = null;
				aantalanswers = 0;
			}
			

			for (var w = 0; w < aantalanswers; w++) {
				var buttontext = document.getElementById(button).textContent;
				var antext = allanswers[w]["text"];
				if (buttontext == antext) {
					answerid = allanswers[w]["id"];
				}
			}
			postQuestion(q, quistionid, answerid);


		}
		catch{

		}
		//teken schema
		drawscheme(aantalvragen, i - 1);

		//haal vorige antwoorden weg
		if (i > 0) {

			var somes = objectLength(Testquiz[i - 1]["answers"]);
			for (var t = 0; t < somes; t++) {
				try {

					var but = document.getElementById(t);
					var bat = document.getElementById("Buttons");

					bat.removeChild(but);
				}
				catch{
				}
			}
		}
		//selecteer de vraag
		var vraag = totalevraag["questionBase"];
		if (vraag == null) {
			vraag = totalevraag["summary"];
		}

		//verrander vraag en voeg nieuwe antwoorden toe aan html pagina
		document.getElementById("vraag").innerHTML = vraag; //vraag uit api
		var aantalvragennu = objectLength(totalevraag["answers"]);
		for (var d = 0; d < aantalvragennu; d++) {
			try {

				var answer = totalevraag["answers"][d]["text"];
				var list = document.getElementById("Buttons");
				var li = document.createElement("li");
				li.appendChild(document.createTextNode(""));
				var button = document.createElement("button");
				button.innerHTML = answer;
				button.setAttribute("id", d);
				button.setAttribute("onclick", "Click(this.id)");
				button.setAttribute("class", "antwoord");
				li.appendChild(button);
				li.setAttribute("id", d);
				list.appendChild(li);
			}
			catch (err) {
			}
		}
		i = i + 1;
		document.getElementById("Next").className = "Hidden";
		if (i == aantalvragen) {
			i = i + 1;
		}
	}
	//Als je klaar bent met de quiz geef aantal goedde antwoorden en sluit af
	if (i == (aantalvragen + 1)) {
		i = i - 1;
		//check laatste vraag
		totalevraag = Testquiz[aantalvragen - 1];
		try {
			quistionid = totalevraag["id"];
			var allanswers;
			var aantalanswers;
			try {
				allanswers = Testquiz[i - 1]["answers"];
				aantalanswers = allanswers["length"];
			}
			catch{
				allanswers = null;
				aantalanswers = 0;
			}

			for (var w = 0; w < aantalanswers; w++) {
				var buttontext = document.getElementById(button).textContent;
				var antext = allanswers[w]["text"];
				if (buttontext == antext) {
					answerid = allanswers[w]["id"];
				}
			}
			postQuestion(q, quistionid, answerid);
		}
		catch{

		}
		//sluit af
		if (a > 1) {
			document.getElementById("vraag").innerHTML = "Einde";
			var para = document.createElement("p");
			var node = document.createTextNode("Aantal goedde antwoorden: " + aantalgoed);
			para.appendChild(node);
			var element = document.getElementById("Buttons");
			element.after(para);
			i = i + 1;
			document.getElementById("Next").className = "";

			$('#Next').text('');
			var next = document.getElementById("Next");
			var link = document.getElementById("Link");
			next.appendChild(link);
		}
	}

	a = a + 1;
}

// krijg de lengte van een object
function objectLength(obj) {
	var result = 0;
	for (var prop in obj) {
		if (obj.hasOwnProperty(prop)) {
			// or Object.prototype.hasOwnProperty.call(obj, prop)
			result++;
		}
	}
	return result;
}

//voeg links naar verschillende quizes toe
function AddQuizes() {

	authenticate_User_andGet_Token();
	var aantald = objectLength(cources);
	for (var i = 0; i < aantald; i++) {
		if (trues == true) {

			var title = cources[i]["title"];
			var ul = document.getElementById("Quizes");
			var li = document.createElement("li");
			li.appendChild(document.createTextNode(""));
			ul.appendChild(li);
			var link = document.createElement("a");
			link.setAttribute("id", cources[i]["id"]);
			link.setAttribute("onclick", "Clickedlink('" + cources[i]["id"] + "')")
			link.appendChild(document.createTextNode(title));
			link.href = "quiz.html"
			li.appendChild(link);
			var elem = document.getElementById("Addquiz");
			elem.parentNode.removeChild(elem);

		}
		else {
			authenticate_User_andGet_Token();
		}
	}
}

//Get user token
function authenticate_User_andGet_Token() {

	$.ajax({
		url: "https://pubquiz.iqualify.nl/api/authenticate",
		beforeSend: function (xhr) {
			xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
		},
		type: 'POST',
		dataType: 'json',
		contentType: 'application/json',
		processData: false,
		data: '{"username" : "mols", "password": "test"}',
		success: function (data) {
			q = data;

			callCourses(data);

		},
		error: function () {
		}
	});
}

//Get specifiek quiz uit api
function callQuizzA(a) {
	var urll = "https://pubquiz.iqualify.nl/api/learnmaterial/" + idofquiz;
	$.ajax({
		url: urll,
		beforeSend: function (xhr) {
			xhr.setRequestHeader('Authorization', 'Bearer ' + a);
		},
		type: 'GET',
		dataType: 'json',
		contentType: 'application/json',
		processData: false,
		data: '{}',
		success: function (data) {
			var q = data;
			Testje = data;
			Testquiz = Testje["pages"];
		},
		error: function (data) {
		}
	});
}

//herstart quiz requist api
function Retryquiz(a) {
	Close(a);
	var urll = "https://pubquiz.iqualify.nl/api/learnmaterial/" + idofquiz + "/retake";
	$.ajax({
		url: urll,
		beforeSend: function (xhr) {
			xhr.setRequestHeader('Authorization', 'Bearer ' + a);
		},
		type: 'POST',
		dataType: 'json',
		contentType: 'application/json',
		processData: false,
		data: '{}',
		success: function (data) {
			var q = data;
			Testje = data;
			Testquiz = Testje["pages"];
		},
		error: function (data) {
		}
	});
}

//sluit quiz requist api
function Close(a) {
	var urll = "https://pubquiz.iqualify.nl/api/learnmaterial/" + idofquiz + "/close";
	$.ajax({
		url: urll,
		beforeSend: function (xhr) {
			xhr.setRequestHeader('Authorization', 'Bearer ' + a);
		},
		type: 'POST',
		dataType: 'json',
		contentType: 'application/json',
		processData: false,
		data: '{}',
		success: function (data) {

		},
		error: function (data) {
		}
	});
}

//roep alle quizes op uit api
function callCourses(a) {

	$.ajax({
		url: "https://pubquiz.iqualify.nl/api/userdata/mycoursesdata",
		beforeSend: function (xhr) {
			xhr.setRequestHeader('Authorization', 'Bearer ' + a);
		},
		type: 'GET',
		dataType: 'json',
		contentType: 'application/json',
		processData: false,
		data: '{}',
		success: function (data) {
			onSucces(data);
			cources = data["courses"]["courses"]["0"]["learnmaterial"];
			idofquiz = localStorage.Id;
			Call(a);

		},
		error: function (data) {
		}
	});
}

//wijst door
function Call(a) {

	callQuizzA(a);
	if (Testquiz == null) {
		Retryquiz(a);
	}

}

//kleine check of quiz getten gelukt is
function onSucces(data) {
	trues = true;
}

//stuur antwoord van vraag naar api
function postQuestion(q, quistionid, answerid) {
	var urll = "https://pubquiz.iqualify.nl/api/learnmaterial/" + localStorage.Id + "/update/" + quistionid;
	//var date = {"confirmed": true, "time": 0, "answer": [answerid]};

	$.ajax({
		url: urll,
		beforeSend: function (xhr) {
			xhr.setRequestHeader('Authorization', 'Bearer ' + q);
		},
		type: 'POST',
		dataType: 'json',
		contentType: 'application/json',
		processData: false,
		data: '{"confirmed": true, "time": 0, "answer": {answerid}}',
		success: function (data) {
			var s = JSON.stringify(data);
			Answerdata = data;
			Answer(answerid, data);
		},
		error: function (jqXHR, textStatus, errorThrown) {
		}
	});
}





//p5
var amoundofplayers = 4;
var question = false;
var playerid = 1;
var heightperplayer;
var widthofstep;
var mywidth;
var myheight;
var Endlinewidth = 0;
var widtha;

function setup() {
	var canvas = createCanvas(801, 401);

	canvas.parent('Visual');

	background(255, 0, 200);
	rect(0, 0, 800, 400);
	var q;
	heightperplayer = 400 / amoundofplayers;
	for (q = 0; q < amoundofplayers; q++) {
		q = q + 1;
		var totheight = q * heightperplayer;
		line(0, totheight, 800, totheight);
		q = q - 1;
	}
}

function drawscheme(aantal, quistionid) {
	var stepstofinish = aantal;
	widthofstep = 800 / stepstofinish;
	var k;
	if (i > 2) {
		endline(i);
	}
	if (i == 0) {
		for (k = 0; k < aantal; k++) {
			var totwidth = k * widthofstep;
			line(totwidth, 0, totwidth, 400);
		}
	}
	else {

		mywidth = widthofstep * aantalgoed;
		myheight = (playerid - 1) * heightperplayer;
		fill(0);
		rect(mywidth, myheight, widthofstep, heightperplayer);
		dc(quistionid);
		fill(0);
		question = false;
		/*if(mywidth <= (Endlinewidth - 100) && i > 1){
		alert("You Failed");
		var canvas = document.getElementById("Visual");
		document.body.removeChild(canvas);
	  }*/
	}

}

function dc(quistionid) {
	if (quistionid > 0) {
		fill(255);
		var min = mywidth - widthofstep;
		rect(min, myheight, widthofstep, heightperplayer);
		fill(255);
	}
}

function endline(a) {
	fill(237, 41, 57);
	widtha = (a - 3) * widthofstep;
	rect(widtha, 0, widthofstep, 400);
	if ((Endlinewidth + 1) > widthofstep) {
		fill(255);

		var width = Endlinewidth - widthofstep;
		rect(width, 0, widthofstep, 400);
		for (q = 0; q < amoundofplayers; q++) {
			q = q + 1;
			var totheight = q * heightperplayer;
			line(0, totheight, 800, totheight);
			q = q - 1;
		}
	}
	Endlinewidth = Endlinewidth + widthofstep;
}

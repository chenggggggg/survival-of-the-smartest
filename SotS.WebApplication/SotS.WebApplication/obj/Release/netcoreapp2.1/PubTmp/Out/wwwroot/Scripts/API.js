﻿var Testquiz;
var Token;
var Data;

//herstart quiz requist api
function Retryquiz(Token) {
	Close(Token);
	var urll = "https://pubquiz.iqualify.nl/api/learnmaterial/" + localStorage.Id + "/retake";
	$.ajax({
		url: urll,
		beforeSend: function (xhr) {
			xhr.setRequestHeader('Authorization', 'Bearer ' + Token);
		},
		type: 'POST',
		dataType: 'json',
		contentType: 'application/json',
		processData: false,
		data: '{}',
		success: function (data) {
			return Testquiz = data["pages"];
		},
		error: function (data) {
		}
	});
}

//sluit quiz requist api
function Close(Token) {
	var urll = "https://pubquiz.iqualify.nl/api/learnmaterial/" + localStorage.Id + "/close";
	$.ajax({
		url: urll,
		beforeSend: function (xhr) {
			xhr.setRequestHeader('Authorization', 'Bearer ' + Token);
		},
		type: 'POST',
		dataType: 'json',
		contentType: 'application/json',
		processData: false,
		data: '{}',
		success: function (data) {

		},
		error: function (data) {
		}
	});
}

//Get specifiek quiz uit api
function callQuizzA(Token) {
	var urll = "https://pubquiz.iqualify.nl/api/learnmaterial/" + localStorage.Id;
	$.ajax({
		url: urll,
		beforeSend: function (xhr) {
			xhr.setRequestHeader('Authorization', 'Bearer ' + Token);
		},
		type: 'GET',
		dataType: 'json',
		contentType: 'application/json',
		processData: false,
		data: '{}',
		success: function (data) {
			var Testje = data;
			return Testquiz = Testje["pages"];
		},
		error: function (data) {
		}
	});
}

//stuur antwoord van vraag naar api
function postQuestion(question_id, answerid, q) {
	var urll = "https://pubquiz.iqualify.nl/api/learnmaterial/" + localStorage.Id + "/update/" + question_id;
	$.ajax({
		url: urll,
		beforeSend: function (xhr) {
			xhr.setRequestHeader('Authorization', 'Bearer ' + q);
		},
		type: 'POST',
		dataType: 'json',
		contentType: 'application/json',
		processData: false,
		data: '{"confirmed": true, "time": 0, "answer": {answerid}}',
		success: function (data) {
			Data = data;
		},
		error: function (jqXHR, textStatus, errorThrown) {
		}
	});
}

//Get user token
function authenticate_User_andGet_Token() {
	var username = localStorage.UserName;
	if (username == "mols") {
		UserMols();
	}
	else if (username == "ster") {
		UserSter();
	}
	else if (username == "loon") {
		UserLoon();
	}
	else if (username == "rhee") {
		UserRhee();
	}
}

//wijst door
function Call(Token) {
	Testquiz = callQuizzA(Token);
	if (Testquiz == null) {
		Testquiz = Retryquiz(Token);
	}
}

function TestQuiz() {
	return Testquiz;
}

function GData() {
	return Data;
}

function GToken() {
	return Token;
}

function UserMols() {
	$.ajax({
		url: "https://pubquiz.iqualify.nl/api/authenticate",
		beforeSend: function (xhr) {
			xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
		},
		type: 'POST',
		dataType: 'json',
		contentType: 'application/json',
		processData: false,
		data: '{ "username": "mols", "password": "test" }',
		success: function (data) {
			callCourses(data);
			Call(data);
			Token = data;
		},
		error: function () {
		}
	});
}

function UserSter() {
	$.ajax({
		url: "https://pubquiz.iqualify.nl/api/authenticate",
		beforeSend: function (xhr) {
			xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
		},
		type: 'POST',
		dataType: 'json',
		contentType: 'application/json',
		processData: false,
		data: '{ "username": "ster", "password": "test" }',
		success: function (data) {
			callCourses(data);
			Call(data);
			Token = data;
		},
		error: function () {
		}
	});
}

function UserLoon() {
	$.ajax({
		url: "https://pubquiz.iqualify.nl/api/authenticate",
		beforeSend: function (xhr) {
			xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
		},
		type: 'POST',
		dataType: 'json',
		contentType: 'application/json',
		processData: false,
		data: '{ "username": "loon", "password": "test" }',
		success: function (data) {
			callCourses(data);
			Call(data);
			Token = data;
		},
		error: function () {
		}
	});
}

function UserRhee() {
	$.ajax({
		url: "https://pubquiz.iqualify.nl/api/authenticate",
		beforeSend: function (xhr) {
			xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
		},
		type: 'POST',
		dataType: 'json',
		contentType: 'application/json',
		processData: false,
		data: '{ "username": "rhee", "password": "test" }',
		success: function (data) {
			callCourses(data);
			Call(data);
			Token = data;
		},
		error: function () {
		}
	});
}
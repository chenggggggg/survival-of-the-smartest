﻿var amountofplayers = 0;
var Users = [
    { id: 0, username: "mols", aantalgoed: 0, connected: false, answered: false, gameover: false },
    { id: 1, username: "ster", aantalgoed: 0, connected: false, answered: false, gameover: false },
    { id: 2, username: "loon", aantalgoed: 0, connected: false, answered: false, gameover: false },
    { id: 3, username: "rhee", aantalgoed: 0, connected: false, answered: false, gameover: false }];
var amountofanswers = 0;
var Currentquestionid;

var connection = new signalR.HubConnectionBuilder().withUrl("/quizHub").build();

connection.start().catch(function (err) {
	return console.error(err.toString());
});

function signalr(user_id, aantalgoed, vraagid, questionAmount) {
	connection.invoke("vraagBeantwoord", user_id, aantalgoed, vraagid, questionAmount).catch(function (err) {
		return console.error(err.toString());
	});
}

connection.on("drawscheme", function (user_id, aantalgoed, vraagid, questionAmount) {
	var id = Users.findIndex(x => x.id === user_id);
	if (Users[id].answered == false) {
		amountofanswers++;
		Users[id].answered = true;

	}

	var q = 0;
	var uniekeantwoorden = 0;
	for (var a in Users) {
		if (Users[q].answered == true) {
			uniekeantwoorden++;
		}
		q++;
	}
	Users[id].aantalgoed = aantalgoed;
	
	if (uniekeantwoorden == Users.length || vraagid == -1) {
		if (amountofanswers >= amountofplayers) {
			amountofanswers = 0;
			var aantalusers = Users.length;
			for (var i = aantalusers - 1; i > -1; i--) {
				if (Users[i].connected == false) {
					Users.splice(i, 1);
				}
			}

			aantalusers = Users.length;
			for (var i = aantalusers - 1; i > -1; i--) {
				Users[i].answered = false;
				localStorage.answered = true;
			}
			draw(Users, vraagid, questionAmount);
		}
	}	
});

function connect(user_id) {
	connection.invoke("Connect", user_id).catch(function (err) {
		return console.error(err.toString());
	});
}

connection.on("connected", function (user_id) {
	if (Users[user_id].connected == false) {
		Users[user_id].connected = true;
		amountofplayers++;
	connect(localStorage.UserId);
	}
});

function updatepoints(username) {
	connection.invoke("updatep", username).catch(function (err) {
		return console.error(err.toString());
	});
}

connection.on("binnen", function (points)
{
});

function Getpoints() {
	connection.invoke("getp", localStorage.UserId).catch(function (err) {
		return console.error(err.toString());
	});
}

connection.on("poins", function (aname1, apoints1, aname2, apoints2, aname3, apoints3, aname4, apoints4,) {
	var p = document.createElement("h3");
	p.innerHTML = aname1 + ": " + apoints1 + " punten";
	document.getElementById("points").appendChild(p);
	p = document.createElement("h3");
	p.innerHTML = aname2 + ": " + apoints2 + " punten";
	document.getElementById("points").appendChild(p);
	p = document.createElement("h3");
	p.innerHTML = aname3 + ": " + apoints3 + " punten";
	document.getElementById("points").appendChild(p);
	p = document.createElement("h3");
	p.innerHTML = aname4 + ": " + apoints4 + " punten";
	document.getElementById("points").appendChild(p);
});
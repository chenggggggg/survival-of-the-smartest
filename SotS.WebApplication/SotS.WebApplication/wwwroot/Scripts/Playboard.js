﻿const yDistance = 60;

var canvas;

var lineOfDeathPosition = 0;
var lineOfDeathPixelPosition = 10;

var currentQuestion = 0;

var thisUsers;

function setup(questionAmount) {
    var canvasHeight = (questionAmount+1) * yDistance + 20;

    canvas = createCanvas(300, canvasHeight);
    canvas.parent('Visual');

    textFont("Arial");
    textAlign(CENTER);
    textSize(20);

    // TODO: Haal de gebruikers en het aantal vragen in de quiz op aan het begin van de quiz.
}

function draw(Users, vraagid, questionAmount) {

	if (vraagid > -1) {
		setup(questionAmount);
		thisUsers = Users;
		background(249, 235, 104);

		drawField(questionAmount);

        drawPlayers(thisUsers);

        //start drawing the line of death when at the 3rd question.
        if (vraagid > 2) {
            lineOfDeathPosition++;
            drawLineOfDeath();
            GameOver();
        }
	}
	var b = 0;
	for (var _u in Users) {
		if (Users[b].aantalgoed >= (questionAmount - 1)) {
			localStorage.FinishedUserId = Users[b].username;
			
			window.location.href = "/Finish";
		}
		b++;
	}
	
}

//Hides the next button to prevent the user of doing any actions while being gameover.
//Sets the user to gameover = true.
function GameOver() {
    if (aantalgoed < lineOfDeathPosition) {
        document.getElementById('Next').style.visibility = 'hidden';
        Users[UserId].gameover = true;
    }
}

function drawField(questions) {
    // Teken het speelveld, iedere horizontale lijn is een vraag.

    var xPoint = 10;
    var xPoint2 = canvas.width - 10;
    var yPoint = 10;

    var i;
    for (i = 0; i < (questions+2); i++) {
        line(xPoint, yPoint, xPoint2, yPoint);

        if (i == 1) {
            line(xPoint, yPoint + 5, xPoint2, yPoint + 5);
        }

        if (i == (questions - 1)) {
            drawFinishLine(xPoint, xPoint2, yPoint);
        }

        yPoint += yDistance;
    }
}

function drawFinishLine(xStart, xEnd, height) {
    var blockWidth = 6;
    var blockHeight = 4;

    var xPoint = xStart;
    var yPoint = height;
    var blackFirst = true;

    while (xPoint < xEnd) {
        if (blackFirst === true) {
            stroke('black');
            fill('black');
        }
        else {
            stroke('white');
            fill('white');
        }

        rect(xPoint, yPoint, blockWidth, blockHeight);

        if (blackFirst === false) {
            stroke('black');
            fill('black');
        }
        else {
            stroke('white');
            fill('white');
        }

        rect(xPoint, yPoint + blockHeight, blockWidth, blockHeight);

        xPoint += blockWidth;
        blackFirst = !blackFirst;
    }

    stroke('black');
    fill('black');
}

function drawPlayers(signalRUsers) {
    // Teken iedere speler op de positie die bij het aantal goede antwoorden hoort.

    var xDistance = canvas.width / signalRUsers.length + 1;
    var yStartPoint = (yDistance / 2) + textSize() / 2 + 10;

    var xPoint = xDistance - xDistance / 2;
    var yPoint;
    

    for (var i = 0; i < signalRUsers.length; i++) {
        yPoint = signalRUsers[i].aantalgoed * yDistance + yStartPoint;

        text(signalRUsers[i].username, xPoint, yPoint);

        xPoint += canvas.width / signalRUsers.length;
    }
}

//Draws the line of death.
function drawLineOfDeath() {
    stroke('red');
    strokeWeight(5);

    lineOfDeathPixelPosition = lineOfDeathPosition * yDistance + 10;

    //Move the position of the line of death by the changed distance.
    line(10, lineOfDeathPixelPosition, canvas.width - 10, lineOfDeathPixelPosition);    
    
    strokeWeight(1);
    stroke('black');
}

//Draws a grey field for everything behind the line of death.
function drawLineOfDeathShade() {
    if (currentQuestion > 1) {
        noStroke();
        fill(181, 105, 5);
        var rectWidth = canvas.width / 2 - 20;
        var rectHeight = lineOfDeathPosition - 10;

        rect(canvas.width / 2 + 10, 10, rectWidth, rectHeight);

        stroke('black');
        strokeWeight(1);
    }
}


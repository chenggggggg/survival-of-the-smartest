﻿var a = 0; // onduidelijk variable geen idee waarom nodig maar anders werkt het einde niet zal fixen

var aantalgoed = 0;

var courses;
//Bewaar id van geselecteerde quiz
function Clickedlink(id) {
    localStorage.Id = id;
}

function Onload() {
    authenticate_User_andGet_Token();
}

//voeg links naar verschillende quizes toe
function AddQuizes() {
    authenticate_User_andGet_Token();


	Getpoints();

    var aantal_courses = objectLength(courses);
    if (aantal_courses == 0) {
        authenticate_User_andGet_Token();
        aantal_courses = objectLength(courses);
    }
    for (var i = 0; i < aantal_courses; i++) {
        var courseID = courses[i]["id"];
        var title = courses[i]["title"];
        var ul = document.getElementById("Quizes");
        var li = document.createElement("li");
        li.appendChild(document.createTextNode(""));
        ul.appendChild(li);
        var link = document.createElement("a");
        link.setAttribute("id", courseID);
        link.setAttribute("onclick", "Clickedlink('" + courseID + "')");
        link.appendChild(document.createTextNode(title));
        link.href = "quiz";
        li.appendChild(link);
        var elem = document.getElementById("Addquiz");
        elem.parentNode.removeChild(elem);
    }
}

//roep alle quizes op uit api
function callCourses(Token) {

    $.ajax({
        url: "https://pubquiz.iqualify.nl/api/userdata/mycoursesdata",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer ' + Token);
        },
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        processData: false,
        data: '{}',
        success: function (data) {
            courses = data["courses"]["courses"]["0"]["learnmaterial"];
        },
        error: function (data) {
        }
    });
}

// krijg de lengte van een object
function objectLength(obj) {
    var result = 0;
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop)) {
            // or Object.prototype.hasOwnProperty.call(obj, prop)
            result++;
        }
    }
    return result;
}



//Begin met quiz nog een keer quiz inladen en token krijgen
function Click(clickedbutton_id) {
    if (clickedbutton_id == 100) {
        localStorage.VraagId = -1;
        document.getElementById('Next').setAttribute("onclick", "Click(0)");
        connect(localStorage.UserId);
    }
    else {
        var ID = 0;
        ID = parseInt(localStorage.VraagId) + 1;
        localStorage.VraagId = ID;
        Redirect(clickedbutton_id, ID);
	}

}

//voeg vragen aan html pagina toe en selecteer gegven antwoord
function Redirect(clickedbutton_id, Vraag_id) {
    var Testquiz = TestQuiz();
    var button = parseInt(clickedbutton_id);
    var aantalvragen = objectLength(Testquiz);

    try {
        //Bij elke vraag doe je dit
        if (Vraag_id < (aantalvragen + 1)) {
            var totalevraag = Testquiz[Vraag_id];
            var question_id
            try {
                question_id = Testquiz[Vraag_id]["id"];
            }
            catch{

            }
        }
        //probeer antwoord te checken
        try {
            var answerid = Check_Answers(Vraag_id, button, Testquiz);
            postQuestion(question_id, answerid, GToken());
            answerid = Check_Answers((Vraag_id - 1), button, Testquiz);
            Answer(answerid);
        }
        catch{

        }

      

		//signalr
		signalr(localStorage.UserId, aantalgoed, (Vraag_id - 1), aantalvragen, connection);
		var a = 1 + 3;
		var b;
		
		
		if (localStorage.answered === "true") {
			localStorage.answered = false;
			//haal vorige antwoorden weg
			Remove_Answers(Vraag_id, Testquiz);

            //verrander vraag en voeg nieuwe antwoorden toe aan html pagina
            AddQuestion(totalevraag);

            document.getElementById("Next").className = "Hidden";
        }
        else {
            localStorage.VraagId = parseInt(localStorage.VraagId) - 1;
        }
    }
    catch{
        Last(a, Vraag_id);
    }
}




function Check_Answers(Vraag_id, button, Testquiz) {
    var answerid;
    try {

        var allanswers;
        var aantalanswers;
        try {
            allanswers = Testquiz[Vraag_id]["answers"];
            aantalanswers = allanswers["length"];
        }
        catch{
            allanswers = null;
            aantalanswers = 0;
        }
        //selecteerd id van geselecteerd antwoord
        for (var w = 0; w < aantalanswers; w++) {
            var buttontext = document.getElementById(button).textContent;
            var answertext = allanswers[w]["text"];
            if (buttontext == answertext) {
                answerid = allanswers[w]["id"];
            }
        }
    }
    catch
    {
    }

    return answerid;
}

function Remove_Answers(Vraag_id, Testquiz) {
    if (Vraag_id > 0) {

        var aantalAntwoorden = objectLength(Testquiz[Vraag_id - 1]["answers"]);
        for (var t = 0; t < aantalAntwoorden; t++) {
            try {

                var button = document.getElementById(t);
                var buttondiv = document.getElementById("Buttons");

                buttondiv.removeChild(button);
            }
            catch{

            }
        }
    }
}

function Add_Answers(totalevraag) {
    var aantalanwoorden = objectLength(totalevraag["answers"]);
    for (var d = 0; d < aantalanwoorden; d++) {
        try {
            var answer = totalevraag["answers"][d]["text"];
            var list = document.getElementById("Buttons");
            var li = document.createElement("li");
            li.appendChild(document.createTextNode(""));
            var button = document.createElement("button");
            button.innerHTML = answer;
            button.setAttribute("id", d);
            button.setAttribute("onclick", "Click(this.id)");
            button.setAttribute("class", "antwoord");
            li.appendChild(button);
            li.setAttribute("id", d);
            list.appendChild(li);
        }
        catch{

        }
    }
}

function AddQuestion(totalevraag) {
	document.getElementById("vraag").innerHTML = "";
	var imglink = "https://pubquiz.iqualify.nl/admin/media/" + totalevraag["media"]["id"];
	document.getElementById("img").setAttribute("src", imglink);
	var questiontype = totalevraag["questionType"];
	var vraag = null;
	if (questiontype == 1) {
		vraag = totalevraag["questionBase"];
		document.getElementById("vraag").innerHTML = vraag;

        var aantalanwoorden = objectLength(totalevraag["answers"]);
        for (var d = 0; d < aantalanwoorden; d++) {
            try {
                var answer = totalevraag["answers"][d]["text"];
                var list = document.getElementById("Buttons");
                var li = document.createElement("li");
                li.appendChild(document.createTextNode(""));
                var button = document.createElement("button");
                button.innerHTML = answer;
                button.setAttribute("id", d);
                button.setAttribute("onclick", "Click(this.id)");
                button.setAttribute("class", "antwoord");
                li.appendChild(button);
                li.setAttribute("id", d);
                list.appendChild(li);
            }
            catch{

            }
        }
    }
}

function Last(a, Vraag_id) {
    // aan het eind van de quiz / na laatste vraag
	localStorage.FinishedUserId = "niemand";
	window.location.href = "/Finish";
}

//check of het gegeven antwoord goed is
function Answer(AnswerId) {
    var data = GData();
    var antwoorden = data["question"]["answers"];
    var aantalantwoorden = objectLength(antwoorden);
    for (var i = 0; i < aantalantwoorden; i++) {
        var correct = antwoorden[i]["correct"];
        if (correct == true) {
            var correctanswer = antwoorden[i]["id"];
            var givenanswer = AnswerId;
            if (correctanswer == givenanswer) {
                aantalgoed++;
                question = true;
            }
        }
    }
}

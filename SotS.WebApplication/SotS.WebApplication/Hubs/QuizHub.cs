﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.IdentityModel.Protocols;

namespace SotS.WebApplication.Hubs
{
    public class QuizHub : Hub
    {

		public async Task vraagBeantwoord(int userida, int aantalgoeda, int vraagid, int aantalvragen)
		{
			await Clients.All.SendAsync("drawscheme", userida, aantalgoeda, vraagid, aantalvragen);
		}

		public async Task Connect(int user_id)
		{
			await Clients.All.SendAsync("connected", user_id);
		}
		public string connectionstring = "Server=mssql.fhict.local;Database=dbi383964;User Id=dbi383964;Password=Schaapje11";
		public SqlConnection connection;

		
		public async Task updatep(string id)
		{
			
			await Clients.All.SendAsync("binnen", id);
			update(id);
			
		}

		private void update(string id)
		{
			int points = 0;
			using (connection = new SqlConnection(connectionstring))
			using (SqlCommand commend = new SqlCommand(
				"SELECT a.points FROM person a WHERE a.Username = @U", connection))
			using (SqlDataAdapter adaptor = new SqlDataAdapter(commend))
			{
				connection.Open();

				commend.Parameters.AddWithValue("@U", id);

				SqlDataReader reader = commend.ExecuteReader();

				if (reader.HasRows)
				{
					while (reader.Read())
					{
						points = Convert.ToInt32( reader["points"]);
					}
				}

			}

			using (connection = new SqlConnection(connectionstring))
			using (SqlCommand commend = new SqlCommand(
				"UPDATE person SET points = @P WHERE Username = @U", connection))
			using (SqlDataAdapter adaptor = new SqlDataAdapter(commend))
			{
				connection.Open();

				commend.Parameters.AddWithValue("@U", id);
				commend.Parameters.AddWithValue("@P", (points + 5));
				

				commend.ExecuteNonQuery();

			}
		}


		string aname1;
		int apoints1;
		string aname2;
		int apoints2;
		string aname3;
		int apoints3;
		string aname4;
		int apoints4;

		public async Task getp(int uid)

		{
			getpoints();
			await Clients.Caller.SendAsync("poins", aname1, apoints1, aname2, apoints2, aname3, apoints3, aname4, apoints4);
		}

		private void getpoints()
		{
			using (connection = new SqlConnection(connectionstring))
			using (SqlCommand commend = new SqlCommand(
				"SELECT a.Username, a.points FROM person a WHERE a.Id = @U", connection))
			using (SqlDataAdapter adaptor = new SqlDataAdapter(commend))
			{
				connection.Open();

				commend.Parameters.AddWithValue("@U", 0);

				SqlDataReader reader = commend.ExecuteReader();

				if (reader.HasRows)
				{
					
					while (reader.Read()){
						aname1 = reader["Username"].ToString();
						apoints1 = Convert.ToInt32(reader["points"]);

					}
				}

			}

			using (connection = new SqlConnection(connectionstring))
			using (SqlCommand commend = new SqlCommand(
				"SELECT a.Username, a.points FROM person a WHERE a.Id = @U", connection))
			using (SqlDataAdapter adaptor = new SqlDataAdapter(commend))
			{
				connection.Open();

				commend.Parameters.AddWithValue("@U", 1);

				SqlDataReader reader = commend.ExecuteReader();

				if (reader.HasRows)
				{

					while (reader.Read())
					{
						aname2 = reader["Username"].ToString();
						apoints2 = Convert.ToInt32(reader["points"]);

					}
				}

			}

			using (connection = new SqlConnection(connectionstring))
			using (SqlCommand commend = new SqlCommand(
				"SELECT a.Username, a.points FROM person a WHERE a.Id = @U", connection))
			using (SqlDataAdapter adaptor = new SqlDataAdapter(commend))
			{
				connection.Open();

				commend.Parameters.AddWithValue("@U", 2);

				SqlDataReader reader = commend.ExecuteReader();

				if (reader.HasRows)
				{

					while (reader.Read())
					{
						aname3 = reader["Username"].ToString();
						apoints3 = Convert.ToInt32(reader["points"]);

					}
				}

			}

			using (connection = new SqlConnection(connectionstring))
			using (SqlCommand commend = new SqlCommand(
				"SELECT a.Username, a.points FROM person a WHERE a.Id = @U", connection))
			using (SqlDataAdapter adaptor = new SqlDataAdapter(commend))
			{
				connection.Open();

				commend.Parameters.AddWithValue("@U", 3);

				SqlDataReader reader = commend.ExecuteReader();

				if (reader.HasRows)
				{

					while (reader.Read())
					{
						aname4 = reader["Username"].ToString();
						apoints4 = Convert.ToInt32(reader["points"]);

					}
				}

			}
		}
	}
}
